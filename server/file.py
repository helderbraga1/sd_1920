from ops import sort_data
from pathlib import Path
import os
import json
class Json_Handler():
    def __init__(self, folder, filename):
        self.folder = folder
        self.filename = filename

    def create_json_time(self):
        folder_mtime = Path(self.folder).stat().st_mtime
        json_data = {"last_update": folder_mtime,
                    "files": []}
        for (each_folder, sp, f) in os.walk(self.folder):
            for file in f:
                if file != self.filename:
                    aux = {}
                    p = each_folder + os.sep + file
                    aux["path"]=p
                    timestamp = Path(p).stat().st_mtime
                    aux["last_changed"]=timestamp
                    json_data["files"].append(aux)

        json_data = sort_data(json_data)
        with open(self.folder + '/' + self.filename, "w") as json_file:
            json.dump(json_data, json_file, indent=4)

    def read_json(self):
        with open(self.folder + '/' + self.filename, "r") as json_read:
            return sort_data(json.load(json_read))

    def get_files(self):
        return self.read_json()['files']

    def get_lastupdate(self):
        return self.read_json()['last_update']