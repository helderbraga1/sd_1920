import json
from sys import platform
from collections import OrderedDict
import shutil
import hashlib
import math

'''
    Receives a path and the json data headers, 
    adds them into a list, and returns the parsed result
'''

def read_json(path):
    with open(path, 'r') as source_file:
        data = json.load(source_file)
    return data

'''
    Filters excess data from a given dictionary collection
'''
def filter_data(headers, data):
    for data_header in data.keys():
        if data_header not in headers:
            data.remove(data_header)
    return data

'''
    Reads a file in binary mode and generates his sha512 checksum
    
    Note: In order to take advantage of the CPU L1 cache speed,
    a maximum size of 4k is recomended for buffering
'''
def read_binary_gen_sha512(path, buff_size_Kb=4):
    first_iteration_flag = True
    #Converts the MB
    buffer_size_bytes = math.ceil(1024*buff_size_Kb)
    with open(path, 'rb', buffering=0) as file_ref:
        sha512 = None
        while True:
            file_data = file_ref.read(buffer_size_bytes)
            if not file_data:
                break
            if first_iteration_flag: 
                sha512 = generate_hash(file_data)
                first_iteration_flag = False
            else:
                sha512.update(file_data)
    return sha512.hexdigest()

'''
    Sorts alphabetically a dictionary by header
'''
def sort_data(data):
    sorted_data = OrderedDict(sorted(data.items(), key=lambda t: t[0]))
    return sorted_data

'''
    Receives the headers, path and data of a json
'''
def write_json(data, path):
    data = sort_data(data)
    data = dict(data)
    with open(path, 'w') as destination_file:
        json.dump(data, destination_file, indent=4)

'''
    Receives the headers, path and data of a json
'''
def write_json_unsorted(data, path):
    with open(path, 'w') as destination_file:
        json.dump(data, destination_file)

'''
    Receives the headers, path and data of a json
'''
def write_json_hash_safe(data, path):
    data = sort_data(data)
    current_file_hash = read_binary_gen_sha512(path)
    current_data_hash = generate_hash(data)
    if current_file_hash != current_data_hash:
        with open(path, 'wb') as destination_file:
            json.dump(data, destination_file)

'''
    Detects the current OS, (Windows, Mac, Linux...)
'''
def detect_os():
    if platform.lower() in 'unix':
        pass
    elif platform.lower in 'win':
        pass
    elif platform.lower in 'posix':
        pass
    else:
        print("Unsuported platform")
        return -1

'''
    Detects the current folder used space
'''
def check_space(path):
    print(shutil.disk_usage(path))

'''
    Generates a unique hash for a given file or string
'''
def generate_hash(data):
    if isinstance(data, str):
        data = data.encode()
    return hashlib.sha512(data)

def write_json_pretty(path):
    data = read_json(path)
    write_json(data, path)

