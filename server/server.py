from ops import generate_hash
import constants
from skeletons.thread import Connection, SubSyncConnection
from queue import Queue
import zmq

class Server():
    def __init__(self, port=2000):
        self.port = int(port)
        self.listeners = {""}
        self.connectionQueue = Queue()
        self.threadCounter = 0

    def set_connection_TCP(self):
        try:
            threaded_connection = Connection()
            threaded_connection.runNewConnectionTCP()
        except:
            print("Failed to create connection")

    #Listens for alterations on the client file structure
    def listener(self, address="127.0.0.1", port=4000):
        if port not in self.listeners:
            self.listeners.add(port)
            subscriber = SubSyncConnection(address, port)
            #if()
            subscriber.start()
            subscriber.join()

    def check_hash(self, message):
        localServerHash = generate_hash(constants.SERVER_STATES_PATH)
        receivedClientStateHash = message
        #Check if client and server file state is equal, if not, update
        if localServerHash.hexdigest() == receivedClientStateHash.hexdigest():
            print("No update needed. Client and Server are in sync.")
            return True
        print("Missmatch! Updating server...")
        print(localServerHash.hexdigest())
        print(receivedClientStateHash.hexdigest())
        print("Update finished!")
        return False

    def insert_in_queue(self):
        try:
            connection = Connection()
            self.connectionQueue.put(connection)
            connection.run()
            print(connection.thread_counter)
            return connection

        except:
            print("Error: Unable to start connection!")

    def iterate_queue(self):
        if not self.connectionQueue.empty():
            currentThread = self.connectionQueue.get()
#            print("Thread ID: ", currentThread.threadID)
            self.connectionQueue.put(currentThread)
            return currentThread
        print("Connection queue empty!")
