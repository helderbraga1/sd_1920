from threading import Thread
from ops import generate_hash, write_json_unsorted
from json import dumps
from constants import CLIENT_STATUS_FILE_LOCAL_COPY
import zmq

'''
    Creates a subscriber connection with ZMQ in a new thread
'''
class SubSyncConnection(Thread):
    def  __init__(self, address, port):
        super(SubSyncConnection, self).__init__()
        self.address = address
        self.port = port
        self.initial_local_client_hash = generate_hash(CLIENT_STATUS_FILE_LOCAL_COPY).hexdigest()

    def run(self):
        context = zmq.Context()
        socket = context.socket(zmq.SUB)
        socket.connect("tcp://"+ self.address + ":%s" % self.port)
        socket.setsockopt_string(zmq.SUBSCRIBE, "sync")
        socket.setsockopt_string(zmq.SUBSCRIBE, "json")
        print("Listening for sync status...")
        while True:
            topic, message = socket.recv_string().split(":::>")
            if topic == "sync":
                if self.initial_local_client_hash != message:
                    print("\nCurrent hash:", self.initial_local_client_hash)
                    print("New hash:    ", message)
                    self.initial_local_client_hash = message
                    topic, message = socket.recv_string().split(":::>")
                    if "json" in topic:
                        print(topic + " data received...")
                        json = message
                        single_quote = "\'"
                        double_quote = "\""
                        parsed_json = json.replace(single_quote,double_quote)
                        parsed_json = json[1:] #Removes first character so it becomes a pure json
                        parsed_json = json[:-1] #Removes last character so it becomes a pure json
                        json = parsed_json
                        print(parsed_json)
                        print(json)
                        write_json_unsorted(message, CLIENT_STATUS_FILE_LOCAL_COPY)
                        new_local_hash = generate_hash(CLIENT_STATUS_FILE_LOCAL_COPY).hexdigest()
                        if new_local_hash == self.initial_local_client_hash:
                            print("SUCCESS: Local copy updated!")
                        else:
                            print("ERROR: The files dont match!!!!")

        socket.close()

class Connection(Thread):
    thread_counter = 0
    def __init__(self):
        thread_counter = +1
        Thread.__init__(self)
        self.thread_id = thread_counter

    def run_new_connection_tcp(self, port=2000):
        print("Starting " + str(self.thread_id))
        context = zmq.Context()
        socket = context.socket(zmq.PUB)
        socket.bind("tcp://*:%s" % port)
        connection = socket
        return connection
