from time import sleep
from threading import Thread
import zmq
import sync
from ops import generate_hash, read_json
import constants
'''
    Creates a publisher connection with ZMQ in a new thread
'''
class PubSyncConnection(Thread):
    def  __init__(self, port):
        super(PubSyncConnection, self).__init__()
        self.port = port
        self.initial_client_hash = generate_hash(constants.CLIENT_STATES_PATH).hexdigest()

    '''
        What the thread does when we run it
    '''
    def run(self):
        context = zmq.Context()
        publisher = context.socket(zmq.PUB)
        connection_try = self.try_connection(publisher, self.port)
        while connection_try == False:
            self.port+=1
            connection_try = self.try_connection(publisher, self.port)
        print('New client connected to port: ' + str(self.port))


        while True:
            watchdog_hash = sync.setup_watcher()
            #Generate the hash of the current client JSON
            message = generate_hash(constants.CLIENT_STATES_PATH)

            #Convert the hash object to string
            hashed = message.hexdigest()

            #Send the hash in string format
            publisher.send_string("%s:::>%s" % ("sync", hashed))

            #Load JSON file to memory
            json = read_json(constants.CLIENT_STATES_PATH)

            #Convert the JSON file on memory to string
            json_string = str(json)

            if self.initial_client_hash != watchdog_hash:
                print("\nCurrent Hash:", self.initial_client_hash)
                print("New Hash:    ", watchdog_hash)
                #Send the JSON file as a string
                publisher.send_string("%s:::>%s" % ("json", json_string))
                self.initial_client_hash = watchdog_hash
            sleep(5)
        publisher.close()

    def try_connection(self,publisher,porta):
        try:
            publisher.bind("tcp://*:%s" % int(porta))
            return True
        except:
            return False


