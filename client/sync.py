import os
from pathlib import Path
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import constants
import shutil
import time
from ops import read_binary_gen_sha512
from file import Json_Handler
import syncbox

'''
    Checks if the files were modified
'''
def compararMod(syncobj, copyobj):
    lista_atualizar = []
    lista_acrescentar = []
    lista_apagar = []
    for i in syncobj.get_files():
        j = i['path'].replace(constants.CLIENT_FOLDER_PATH, constants.SERVER_STATUS_FILE_PATH)
        x = Path(j).exists()
        if x:
            if Path(i['path']).stat().st_mtime > Path(j).stat().st_mtime:
                lista_atualizar.append(i['path'])
        else:
            lista_acrescentar.append(i['path'])

    for k in copyobj.get_files():
        m = k['path'].replace(constants.SERVER_STATUS_FILE_PATH, constants.CLIENT_FOLDER_PATH)
        if Path(m).exists() == False:
            lista_apagar.append(k['path'])

    if lista_acrescentar.__len__() > 0:
        print('Ficheiros a acrescentar: ' + str(lista_acrescentar))
    if lista_atualizar.__len__() > 0:
        print('Ficheiros a atualizar: ' + str(lista_atualizar))
    if lista_apagar.__len__() > 0:
        print('Ficheiros a apagar: ' + str(lista_apagar))

    dict_alteracoes = {'Add': lista_acrescentar, 'Update': lista_atualizar, 'Delete': lista_apagar}

    return dict_alteracoes

'''
    Syncs changes from the client to the server folders
'''
def sincronizar(dict_alterar):
    for i in dict_alterar['Add']:
        os.makedirs(os.path.dirname(i.replace(constants.CLIENT_FOLDER_PATH, constants.SERVER_STATUS_FILE_PATH)), exist_ok=True)
        shutil.copy(i,i.replace(constants.CLIENT_FOLDER_PATH, constants.SERVER_STATUS_FILE_PATH))
        print('Ficheiros adicionados com sucesso!')

    for j in dict_alterar['Update']:
        shutil.copy(j, j.replace(constants.CLIENT_FOLDER_PATH, constants.SERVER_STATUS_FILE_PATH))
        print('Ficheiros atualizados com sucesso!')

    for k in dict_alterar['Delete']:
        os.remove(k)
        print('Ficheiros apagados com sucesso!')

    syncbox.main()


status_file = Json_Handler(constants.CLIENT_STATUS_FILE_PATH, constants.CLIENT_STATUS_FILE_NAME)
status_file.create_json_time()
server_status_file= Json_Handler(constants.SERVER_STATUS_FILE_PATH, constants.SERVER_STATUS_FILE_NAME)
server_status_file.create_json_time()
class MyHandler(FileSystemEventHandler):
    def __init__(self):
        self.last_status_hash = read_binary_gen_sha512(constants.CLIENT_STATUS_FILE_PATH + '/' + constants.CLIENT_STATUS_FILE_NAME)

    def on_any_event(self, event):
        if event.src_path != constants.CLIENT_FOLDER_PATH+'\\' + constants.CLIENT_STATUS_FILE_NAME:
            status_file.create_json_time()
            server_status_file.create_json_time()
            alteracoes = compararMod(status_file,server_status_file)
            sincronizar(alteracoes)
            self.last_status_hash = read_binary_gen_sha512(constants.CLIENT_STATUS_FILE_PATH + '/' + constants.CLIENT_STATUS_FILE_NAME)

def setup_watcher():
    print("Starting the watch *.*")
    first_status_hash = read_binary_gen_sha512(constants.CLIENT_STATUS_FILE_PATH + '/' + constants.CLIENT_STATUS_FILE_NAME)
    event_handler = MyHandler()
    observer = Observer()
    observer.schedule(event_handler, constants.CLIENT_FOLDER_PATH, recursive=True)
    observer.start()
    try:
        while True:
            new_client_hash = event_handler.__getattribute__("last_status_hash")
            if first_status_hash != new_client_hash:
                observer.stop()
                return new_client_hash
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()