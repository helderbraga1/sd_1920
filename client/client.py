from time import sleep
import zmq

import constants
import datetime
from threading import Thread
from ops import generate_hash
from ops import read_json
import sync
from file import Json_Handler
from thread import PubSyncConnection

class Client():
    def __init__(self, address="127.0.0.1", port=2000):
        self.address = address
        self.port = int(port)
        self.publishers = {""}
        self.connection = None
        self.connectionQueue = None

    def connectModeTCP(self, header="0"):
        context = zmq.Context()
        socket = context.socket(zmq.SUB)
        socket.connect("tcp://"+ self.address + ":%s" % self.port)
        socket.setsockopt_string(zmq.SUBSCRIBE, header)
        self.connection = socket

    '''
        Pub/Sub model for client side filesystem changes
        the client side is the publisher
    '''
    def sync_pub(self, port=4000):
        if port not in self.publishers:
            self.publishers.add(port)
            publisher = PubSyncConnection(port)
            publisher.start()
            #publisher.join()

    def startListening(self, checkInterval=0):
        topic, messagedata = self.connection.recv().split()
        topic = topic.decode()
        messagedata = messagedata.decode()
        date = datetime.datetime.now().date()
        time = datetime.datetime.now().time()
        timestamp = "["+ date.strftime('%d/%m/%Y') +" " + time.strftime('%H:%M:%S') + "]"
        print(timestamp, topic , messagedata)

    def start_watcher(self):
        print('Starting the watch! *.*')
        return sync.setup_watcher()

    def send_sync_hash(self):
        status_file = Json_Handler(constants.CLIENT_STATUS_FILE_PATH, constants.CLIENT_STATUS_FILE_NAME)
        status_file.create_json_time()
